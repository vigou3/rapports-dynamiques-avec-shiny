<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Rapports dynamiques avec Shiny

[Shiny](https://shiny.rstudio.com) est une technologie de RStudio qui permet de créer facilement des applications web dynamiques dont les calculs sont effectués par R. Cela permet, entre autres, de créer des rapports interactifs de plus en plus populaires en milieu de travail.

[*Rapports dynamiques avec
Shiny*](https://gitlab.com/vigou3/rapports-dynamiques-avec-shiny/releases/) est un laboratoire
(ou atelier) d'introduction à Shiny offert dans le cadre du cours [Méthodes numériques en actuariat](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2002-methodes-numeriques-en-actuariat.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

Le laboratoire consiste en une présentation entrecoupée d'exemples et
d'exercices visant à reproduire l'application `shortfall`.

Vous pouvez obtenir une narration pour accompagner les diapositives en visionnant les vidéos de la [chaine YouTube](https://www.youtube.com/user/VincentGouletACT2002/).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Rapports dynamiques avec Shiny» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`rapports-dynamiques-avec-shiny`](https://gitlab.com/vigou3/rapports-dynamiques-avec-shiny) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`rapports-dynamiques-avec-shiny-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/rapports-dynamiques-avec-shiny-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `rapports-dynamiques-avec-shiny-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
